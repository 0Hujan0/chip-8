#include "instructions.h"
#include <stdlib.h>
#include <stdio.h>

byte get_low_byte_low_nibble(word instruction)
{
    byte result = instruction.low & 0xF;
    return result;
}

byte get_low_byte_high_nibble(word instruction)
{
    byte result = (instruction.low & 0xF0) >> 4;
    return result;
}

byte get_high_byte_low_nibble(word instruction)
{
    byte result = instruction.high & 0xF;
    return result;
}

byte get_high_byte_high_nibble(word instruction)
{
    byte result = (instruction.high & 0xF0) >> 4;
    return result;
}

dword as_nibbles(word instruction)
{
    dword result;
    result.low.low = instruction.low & 0xF;
    result.low.high = (instruction.low & 0xF0) >> 4;
    result.high.low = instruction.high & 0xF;
    result.high.high = (instruction.high & 0xF0) >> 4;
    return result;
}

byte byte_from_nibbles(byte high, byte low)
{
    byte result = low;
    result += high << 4;
    return result;
}
word word_from_nibbles(byte high_high, byte high_low, byte low_high, byte low_low)
{
    word result;
    result.low = low_low;
    result.low += low_high << 4;
    result.high = high_low;
    result.high += high_high << 4;
    return result;
}

void step(chip8 *machine, byte keypress)
{
    instruction inst;
    inst.high = machine->memory.buffer [ machine->pc.value++ ];
    inst.low = machine->memory.buffer [ machine->pc.value++ ];
    dword nibbles = as_nibbles(inst);
    //printf("step: pc(0x%x) %x %x %x %x\n", machine->pc.value, nibbles.high.high, nibbles.high.low, nibbles.low.high, nibbles.low.low);
    //printf("registers:\n");
    //for (byte i=0; i< 0x10; i++)
    //{
    //    printf("V%x : 0x%x\n", i, machine->registers.buffer[i]);
    //}
    //dword i_nibbles = as_nibbles(machine->index);
    //printf("I : (%x) %x %x %x\n", i_nibbles.high.high, i_nibbles.high.low, i_nibbles.low.high, i_nibbles.low.low);
    //printf("delay_timer : 0x%x\n", machine->timers.delay);
    //if (machine->timers.delay) machine->timers.delay -= 1; //TODO:make the timer proper
    //printf("stack_ptr : 0x%x\n", machine->stack_ptr);
    //printf("stack:\n");
    //for (byte i=0; i< machine->stack_ptr; i++)
    //{
    //    printf("%x : 0x%x\n", i, machine->stack.levels[i].value);
    //}

    switch (nibbles.high.high)
    {
        case 0:
            // 0nnn - Not implemented
            if (inst.low == 0xE0)
            {
                byte rows = sizeof(machine->display.rows) / sizeof(display_row);
                byte cols = sizeof(display_row);
                for(byte y = 0; y < rows; y++)
                {
                    for (byte x = 0; x < cols; x++)
                    {
                        machine->display.rows[y].pixels[x] = 0;
                    }
                }
            }
            if (inst.low == 0xEE)
            {
                machine->pc = machine->stack.levels[machine->stack_ptr - 1];
                machine->stack_ptr--;
            }
            break;
        case 1:
            machine->pc = word_from_nibbles(0, nibbles.high.low, nibbles.low.high, nibbles.low.low);
            break;
        case 2:
            machine->stack_ptr ++;
            machine->stack.levels[machine->stack_ptr - 1] = machine->pc;
            machine->pc = word_from_nibbles(0, nibbles.high.low, nibbles.low.high, nibbles.low.low);
            break;
        case 3:
            if (machine->registers.buffer[nibbles.high.low] == inst.low)
            {
                machine->pc.value += 2;
            }
            break;
        case 4:
            if (machine->registers.buffer[nibbles.high.low] != inst.low)
            {
                machine->pc.value += 2;
            }
            break;
        case 5:
            if (machine->registers.buffer[nibbles.high.low] == machine->registers.buffer[nibbles.low.high])
            {
                machine->pc.value += 2;
            }
            break;
        case 6:
            machine->registers.buffer[nibbles.high.low] = inst.low;
            break;
        case 7:
            machine->registers.buffer[nibbles.high.low] += inst.low;
            break;
        case 8:
            switch(nibbles.low.low)
            {
                word tmp;
                case 0:
                    machine->registers.buffer[nibbles.high.low] = machine->registers.buffer[nibbles.low.high];
                    break;
                case 1:
                    machine->registers.buffer[nibbles.high.low] |= machine->registers.buffer[nibbles.low.high];
                    break;
                case 2:
                    machine->registers.buffer[nibbles.high.low] &= machine->registers.buffer[nibbles.low.high];
                    break;
                case 3:
                    machine->registers.buffer[nibbles.high.low] ^= machine->registers.buffer[nibbles.low.high];
                    break;
                case 4:
                    tmp.value = machine->registers.buffer[nibbles.high.low] + machine->registers.buffer[nibbles.low.high];
                    machine->registers.buffer[nibbles.high.low] = tmp.low;
                    machine->registers.carry = tmp.high;
                    break;
                case 5:
                    tmp.high = 1;
                    tmp.low = machine->registers.buffer[nibbles.high.low];
                    tmp.value -= machine->registers.buffer[nibbles.low.high];
                    machine->registers.carry = tmp.high;
                    machine->registers.buffer[nibbles.high.low] = tmp.low;
                    break;
                case 6:
                    machine->registers.carry = machine->registers.buffer[nibbles.high.low] & 1;
                    machine->registers.buffer[nibbles.high.low] >>= 1;
                    break;
                case 7:
                    tmp.high = 1;
                    tmp.low = machine->registers.buffer[nibbles.low.high];
                    tmp.value -= machine->registers.buffer[nibbles.high.low];
                    machine->registers.carry = tmp.high;
                    machine->registers.buffer[nibbles.high.low] = tmp.low;
                    break;
                case 0xE:
                    machine->registers.carry = ( machine->registers.buffer[nibbles.high.low] & 0x80 ) > 0;
                    machine->registers.buffer[nibbles.high.low] <<= 1;
                    break;
            }
            break;
        case 9:
            if (machine->registers.buffer[nibbles.high.low] != machine->registers.buffer[nibbles.low.high])
            {
                machine->pc.value += 2;
            }
            break;
        case 0xA:
            machine->index = word_from_nibbles(0, nibbles.high.low, nibbles.low.high, nibbles.low.low);
            break;
        case 0xB:
            machine->pc = word_from_nibbles(0, nibbles.high.low, nibbles.low.high, nibbles.low.low);
            machine->pc.value += machine->registers.v0;
            break;
        case 0xC:
            byte random_number = rand() % 256;
            machine->registers.buffer[nibbles.high.low] = random_number & inst.low;
            break;
        case 0xD:
            byte x = machine->registers.buffer[nibbles.high.low];
            byte y = machine->registers.buffer[nibbles.low.high];
            byte n = nibbles.low.low;
            byte rows = sizeof(machine->display.rows) / sizeof(display_row);
            byte cols = sizeof(display_row);
            word index = machine->index;
            machine->registers.carry=0;
            for(byte i = 0; i < n; i++)
            {
                for (byte b = 0; b < 8; b++)
                {
                    byte *pixel = machine->display.rows[(y+i) % rows].pixels + ((x + b) % cols);
                    byte sprite_value = (machine->memory.buffer[index.value + i] & (0x80 >> b)) > 1;
                    if (sprite_value && *pixel)
                    {
                        machine->registers.carry = 1;
                    }
                    *pixel ^= sprite_value;
                }
            }
            break;
        case 0xE:
            switch(inst.low)
            {
                case 0x9E:
                    if (machine->keypad.buffer[machine->registers.buffer[nibbles.high.low]])
                    {
                        machine->pc.value += 2;
                    }
                    break;
                case 0xA1:
                    if (!machine->keypad.buffer[machine->registers.buffer[nibbles.high.low]])
                    {
                        machine->pc.value += 2;
                    }
                    break;
            }
            break;
        case 0xF:
            switch(inst.low)
            {
                word index;
                case 7:
                    machine->registers.buffer[nibbles.high.low] = machine->timers.delay;
                    break;
                case 0x0A:
                    if (keypress)
                    {
                        machine->registers.buffer[nibbles.high.low] = keypress-1;
                    }
                    else
                    {
                        machine->pc.value -= 2;
                    }
                    break;
                case 0x15:
                    machine->timers.delay = machine->registers.buffer[nibbles.high.low];
                    break;
                case 0x18:
                    machine->timers.sound = machine->registers.buffer[nibbles.high.low];
                    break;
                case 0x1E:
                    machine->index.value += machine->registers.buffer[nibbles.high.low];
                    break;
                case 0x29:
                    machine->index.value = machine->registers.buffer[nibbles.high.low] * 5;
                    break;
                case 0x33:
                    index = machine->index;
                    byte value = machine->registers.buffer[nibbles.high.low];
                    machine->memory.buffer[index.value + 2] = value % 10;
                    value /= 10;
                    machine->memory.buffer[index.value + 1] = value % 10;
                    value /= 10;
                    machine->memory.buffer[index.value] = value;
                    break;
                case 0x55:
                    index = machine->index;
                    for (byte x = 0; x <= nibbles.high.low; x++)
                    {
                        machine->memory.buffer[index.value++ ] = machine->registers.buffer[x];
                    }
                    break;
                case 0x65:
                    index = machine->index;
                    for (byte x = 0; x <= nibbles.high.low; x++)
                    {
                        machine->registers.buffer[x] = machine->memory.buffer[index.value++ ];
                    }
                    break;
            }
            break;
        default:
            printf("Not implemented! : pc(0x%x) %x %x %x %x\n", machine->pc.value, nibbles.high.high, nibbles.high.low, nibbles.low.high, nibbles.low.low);
            exit(1);
    }
}

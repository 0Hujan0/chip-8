all: main.c initialise.c instructions.c display.c
	gcc main.c initialise.c instructions.c display.c -o chip8 -lSDL2 -lm

clean: chip8
	rm chip8

run: all
	./chip8


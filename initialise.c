#include "initialise.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void initialise_font(chip8 * machine)
{
    byte font[0x50] =
    {
        // 0
        0b11110000,
        0b10010000,
        0b10010000,
        0b10010000,
        0b11110000,

        // 1
        0b00100000,
        0b01100000,
        0b00100000,
        0b00100000,
        0b01110000,

        // 2
        0b11110000,
        0b00010000,
        0b11110000,
        0b10000000,
        0b11110000,

        // 3
        0b11110000,
        0b00010000,
        0b11110000,
        0b00010000,
        0b11110000,

        // 4
        0b10010000,
        0b10010000,
        0b11110000,
        0b00010000,
        0b00010000,

        // 5
        0b11110000,
        0b10000000,
        0b11110000,
        0b00010000,
        0b11110000,

        // 6
        0b11110000,
        0b10000000,
        0b11110000,
        0b10010000,
        0b11110000,

        // 7
        0b11110000,
        0b00010000,
        0b00100000,
        0b01000000,
        0b01000000,

        // 8
        0b11110000,
        0b10010000,
        0b11110000,
        0b10010000,
        0b11110000,

        // 9
        0b11110000,
        0b10010000,
        0b11110000,
        0b00010000,
        0b11110000,

        // A
        0b11110000,
        0b10010000,
        0b11110000,
        0b10010000,
        0b10010000,

        // B
        0b11100000,
        0b10010000,
        0b11100000,
        0b10010000,
        0b11100000,

        // C
        0b11110000,
        0b10000000,
        0b10000000,
        0b10000000,
        0b11110000,

        // D
        0b11100000,
        0b10010000,
        0b10010000,
        0b10010000,
        0b11100000,

        // E
        0b11110000,
        0b10000000,
        0b11110000,
        0b10000000,
        0b11110000,

        // F
        0b11110000,
        0b10000000,
        0b11110000,
        0b10000000,
        0b10000000,
    };
    for (int i = 0; i<0x50; i++)
    {
        machine->memory.font[i] = font[i];
    }
}

void zero_everything(chip8 *machine)
{
    int i=0;
    for (i = 0; i < sizeof(machine->registers); i++)
    {
        machine->registers.buffer[i] = 0;
    }
    for (i = 0; i < sizeof(machine->memory); i++)
    {
        machine->memory.buffer[i] = 0;
    }
    machine->pc.value = 0x200;
    machine->index.value = 0;
    machine->stack_ptr = 0;
    for (i = 0; i < sizeof(machine->stack) / sizeof(word); i++)
    {
        machine->stack.levels[i].value = 0;
    }
    machine->timers.delay = 0;
    machine->timers.sound = 0;
    for (int y = 0; y < sizeof(machine->display.rows) / sizeof(display_row); y++)
    {
        for (int x = 0; x < sizeof(display_row); x++)
        {
            machine->display.rows[y].pixels[x] = 0;
        }
    }
    for (i = 0; i < sizeof(machine->keypad); i++)
    {
        machine->keypad.buffer[i] = 0;
    }
}

void read_rom(chip8 *machine, char *filename)
{
    FILE *file;
    long len;
    long available = sizeof(machine->memory.ram);

    file = fopen(filename, "rb");

    if (file == NULL)
    {
        printf("File does not exist: %s\n", filename);
        exit(1);
    }
    fseek(file, 0, SEEK_END);
    len = ftell(file);
    rewind(file);

    len = len <= available ? len : available;
    fread(machine->memory.ram, len, 1, file);

    fclose(file);
}

void initialise(chip8 *machine, char *filename)
{
    srand(time(0));
    zero_everything(machine);
    initialise_font(machine);
    read_rom(machine, filename);
}

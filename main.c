#include <stdio.h>
#include <math.h>
#include "datatypes.h"
#include "initialise.h"
#include "instructions.h"
#include "display.h"
#include "SDL2/SDL.h"

void audio_callback(void* userdata, Uint8* stream, int len)
{
    int v = 0;
    if (!*(byte*)userdata)
    {
        for (int i=0; i < len; i++)
        {
            stream[i] = 0;
        }
        return;
    }
    for (int i=0; i < len; i++)
    {
        v += 440;
        stream[i] = (int) (10 * sin( 2 * M_PI * v / 44100 )) % 128 + 128;
    }
}

int main(int argc, char* argv[])
{
    chip8 machine;
    int running = 1;
    SDL_Event event;
    sdl_display display;
    display.window = NULL;
    display.renderer = NULL;
    int return_code = 0;
    int emulation_freq = 500;

    if (argc < 2)
    {
        printf("Need file argument\n");
        return 1;
    }

    initialise(&machine, argv[1]);

    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        printf("Could not initialise SDL. (%s)\n", SDL_GetError());
        return_code = 1;
        goto done;
    }

    if (initialise_display(&display))
    {
        printf("Could not create Window. (%s)\n", SDL_GetError());
        return_code = 2;
        goto done;
    }

    SDL_AudioSpec want, have;
    SDL_AudioDeviceID dev;

    SDL_memset(&want, 0, sizeof(want));
    want.freq = 44100;
    want.format = AUDIO_U8;
    want.channels = 1;
    want.samples = 1024;
    want.callback = audio_callback;
    want.userdata = (void*) &(machine.timers.sound);
    dev = SDL_OpenAudioDevice(NULL, 0, &want, &have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);

    if (dev < 0)
    {
        printf("Audio initialisation failed, starting without audio. (%s)\n", SDL_GetError());
    }

    SDL_PauseAudioDevice(dev, 0);

    Uint32 ticks = SDL_GetTicks();
    Uint32 timer_ticks = SDL_GetTicks();
    Uint32 step_ticks = SDL_GetTicks();
    while(running)
    {
        byte keypress = 0;
        ticks = SDL_GetTicks();

        if ( ticks - timer_ticks >= 16 )
        {
            timer_ticks = SDL_GetTicks();
            if (machine.timers.delay > 0)
                machine.timers.delay --;
            if (machine.timers.sound > 0)
                machine.timers.sound --;
        }

        clear_screen(&display);
        for (int y = 0; y < sizeof(machine.display.rows) / sizeof(display_row); y++)
        {
            for (int x = 0; x < sizeof(display_row); x++)
            {
                draw(&display, x, y, machine.display.rows[y].pixels[x]);
            }
        }

        while( SDL_PollEvent(&event) )
        {
            switch( event.type )
            {
                case SDL_QUIT:
                    running = 0;
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_1:
                            machine.keypad.buffer[1] = 1;
                            keypress = 2;
                            break;
                        case SDLK_2:
                            machine.keypad.buffer[2] = 1;
                            keypress = 3;
                            break;
                        case SDLK_3:
                            machine.keypad.buffer[3] = 1;
                            keypress = 4;
                            break;
                        case SDLK_4:
                            machine.keypad.buffer[0xC] = 1;
                            keypress = 0xD;
                            break;
                        case SDLK_q:
                            machine.keypad.buffer[4] = 1;
                            keypress = 5;
                            break;
                            break;
                        case SDLK_w:
                            machine.keypad.buffer[5] = 1;
                            keypress = 6;
                            break;
                        case SDLK_e:
                            machine.keypad.buffer[6] = 1;
                            keypress = 7;
                            break;
                        case SDLK_r:
                            machine.keypad.buffer[0xD] = 1;
                            keypress = 0xE;
                            break;
                        case SDLK_a:
                            machine.keypad.buffer[7] = 1;
                            keypress = 8;
                            break;
                        case SDLK_s:
                            machine.keypad.buffer[8] = 1;
                            keypress = 9;
                            break;
                        case SDLK_d:
                            machine.keypad.buffer[9] = 1;
                            keypress = 10;
                            break;
                        case SDLK_f:
                            machine.keypad.buffer[0xE] = 1;
                            keypress = 0xF;
                            break;
                        case SDLK_z:
                            machine.keypad.buffer[0xA] = 1;
                            keypress = 0xB;
                            break;
                        case SDLK_x:
                            machine.keypad.buffer[0] = 1;
                            keypress = 0x1;
                            break;
                        case SDLK_c:
                            machine.keypad.buffer[0xB] = 1;
                            keypress = 0xC;
                            break;
                        case SDLK_v:
                            machine.keypad.buffer[0xF] = 1;
                            keypress = 0x10;
                            break;
                    }
                    break;
                case SDL_KEYUP:
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_1:
                            machine.keypad.buffer[1] = 0;
                            break;
                        case SDLK_2:
                            machine.keypad.buffer[2] = 0;
                            break;
                        case SDLK_3:
                            machine.keypad.buffer[3] = 0;
                            break;
                        case SDLK_4:
                            machine.keypad.buffer[0xC] = 0;
                            break;
                        case SDLK_q:
                            machine.keypad.buffer[4] = 0;
                            break;
                        case SDLK_w:
                            machine.keypad.buffer[5] = 0;
                            break;
                        case SDLK_e:
                            machine.keypad.buffer[6] = 0;
                            break;
                        case SDLK_r:
                            machine.keypad.buffer[0xD] = 0;
                            break;
                        case SDLK_a:
                            machine.keypad.buffer[7] = 0;
                            break;
                        case SDLK_s:
                            machine.keypad.buffer[8] = 0;
                            break;
                        case SDLK_d:
                            machine.keypad.buffer[9] = 0;
                            break;
                        case SDLK_f:
                            machine.keypad.buffer[0xE] = 0;
                            break;
                        case SDLK_z:
                            machine.keypad.buffer[0xA] = 0;
                            break;
                        case SDLK_x:
                            machine.keypad.buffer[0] = 0;
                            break;
                        case SDLK_c:
                            machine.keypad.buffer[0xB] = 0;
                            break;
                        case SDLK_v:
                            machine.keypad.buffer[0xF] = 0;
                            break;
                    }
                    break;
            }
        }
        SDL_RenderPresent(display.renderer);
        if ( ticks - step_ticks >= 1000/emulation_freq )
        {
            step_ticks = SDL_GetTicks();
            step(&machine, keypress);
        }
    }

done:
    destroy_display(&display);
    SDL_CloseAudioDevice(dev);
    SDL_Quit();
    return return_code;
}

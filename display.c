#include "display.h"

int initialise_display(sdl_display *display)
{
    display->renderer = NULL;
    display->window = NULL;
    int ret = 0;
    if (ret = SDL_CreateWindowAndRenderer(800, 600, 0, &(display->window), &(display->renderer)) < 0)
    {
        return ret;
    }
    display->real_position.x = 10;
    display->real_position.y = 10;
    display->real_position.w = 640;
    display->real_position.h = 320;
    display->emulator_size.x = 0;
    display->emulator_size.y = 0;
    display->emulator_size.w = 64;
    display->emulator_size.h = 32;
    display->pixel.x = 0;
    display->pixel.y = 0;
    display->pixel.w = display->real_position.w / display->emulator_size.w;
    display->pixel.h = display->real_position.h / display->emulator_size.h;
    display->fg_colour.r = 255;
    display->fg_colour.g = 255;
    display->fg_colour.b = 255;
    display->fg_colour.a = 255;
    display->bg_colour.r = 0;
    display->bg_colour.g = 0;
    display->bg_colour.b = 0;
    display->bg_colour.a = 0;
    return 0;
}

void destroy_display(sdl_display *display)
{
    if (display == NULL)
    {
        return;
    }
    SDL_DestroyRenderer(display->renderer);
    SDL_DestroyWindow(display->window);
}

void clear_screen(sdl_display *display)
{
    SDL_SetRenderDrawColor(display->renderer,
            display->bg_colour.r,
            display->bg_colour.g,
            display->bg_colour.b,
            display->bg_colour.a);
    SDL_RenderClear(display->renderer);
    SDL_SetRenderDrawColor(display->renderer,
            display->fg_colour.r,
            display->fg_colour.g,
            display->fg_colour.b,
            display->fg_colour.a);
    SDL_Rect border = {
        .x = display->real_position.x - 1,
        .y = display->real_position.y - 1,
        .w = display->real_position.w + 2,
        .h = display->real_position.h + 2};
    SDL_RenderFillRect(display->renderer, &border);
}

void draw(sdl_display *display, int x, int y, int fg)
{
    if(fg)
    {
    SDL_SetRenderDrawColor(display->renderer,
            display->fg_colour.r,
            display->fg_colour.g,
            display->fg_colour.b,
            display->fg_colour.a);
    }
    else
    {
    SDL_SetRenderDrawColor(display->renderer,
            display->bg_colour.r,
            display->bg_colour.g,
            display->bg_colour.b,
            display->bg_colour.a);
    }

    display->pixel.x = x * display->pixel.w + display->real_position.x;
    display->pixel.y = y * display->pixel.h + display->real_position.y;

    SDL_RenderFillRect(display->renderer, &(display->pixel));
}

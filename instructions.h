#pragma once
#include "datatypes.h"

byte get_low_byte_low_nibble(word instruction);
byte get_low_byte_high_nibble(word instruction);
byte get_high_byte_low_nibble(word instruction);
byte get_high_byte_high_nibble(word instruction);
dword as_nibbles(word instruction);

byte byte_from_nibbles(byte high, byte low);
word word_from_nibbles(byte high_high, byte high_low, byte low_high, byte low_low);

void step(chip8 *machine, byte keypress);

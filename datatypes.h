#pragma once
typedef unsigned char byte;

typedef union
{
    unsigned short value;
    struct
    {
        byte low;
        byte high;
    };
} word;

typedef union
{
    unsigned int value;
    struct
    {
        word low;
        word high;
    };
} dword;

typedef word instruction;

typedef union
{
    struct
    {
        byte v0;
        byte v1;
        byte v2;
        byte v3;
        byte v4;
        byte v5;
        byte v6;
        byte v7;
        byte v8;
        byte v9;
        byte va;
        byte vb;
        byte vc;
        byte vd;
        byte ve;
        union{ byte vf; byte carry; };
    };
    struct
    {
        byte buffer[16];
    };
 } registers;

typedef union
{
    struct
    {
        union
        {
            byte font[0x50];
            struct
            {
                byte _0[5];
                byte _1[5];
                byte _2[5];
                byte _3[5];
                byte _4[5];
                byte _5[5];
                byte _6[5];
                byte _7[5];
                byte _8[5];
                byte _9[5];
                byte _a[5];
                byte _b[5];
                byte _c[5];
                byte _d[5];
                byte _e[5];
                byte _f[5];
            };
        };
        byte post_font[0x1B0];
        byte ram[0xE00];
    };
    byte buffer [0x1000];
} memory;

typedef struct
{
    word levels[0xF];
} stack;

typedef struct
{
    byte delay;
    byte sound;
} timers;

typedef struct
{
    byte status[0xF];
} keys;

typedef struct
{
    byte pixels[0x40];
} display_row;

typedef struct
{
    union
    {
        display_row rows[0x20];
        byte buffer[800];
    };
} display_t;

typedef struct
{
    registers registers;
    memory memory;
    word pc;
    word index;
    byte stack_ptr;
    stack stack;
    timers timers;
    display_t display;
    registers keypad;
} chip8;

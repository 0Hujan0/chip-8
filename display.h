#pragma once
#include "SDL2/SDL.h"

typedef struct
{
    SDL_Renderer *renderer;
    SDL_Window *window;
    SDL_Rect real_position;
    SDL_Rect emulator_size;
    SDL_Rect pixel;
    struct {
        Uint8 r;
        Uint8 g;
        Uint8 b;
        Uint8 a;
    } fg_colour;
    struct {
        Uint8 r;
        Uint8 g;
        Uint8 b;
        Uint8 a;
    } bg_colour;
} sdl_display;

int initialise_display(sdl_display *display);
void destroy_display(sdl_display *display);

void clear_screen(sdl_display *display);
void draw(sdl_display *display, int x, int y, int fg);
